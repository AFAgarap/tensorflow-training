### getting-started

This component contains the Jupyter Notebooks for lectures on the following materials:
* Mathematics for Machine Learning
* Brief Introduction to Artificial Intelligence & Machine Learning
* Softmax Regression for MNIST Classification
* Convolutional Neural Network (CNN) for MNIST Classification
* Multilayer Perceptron for MNIST Classification
* Recurrent Neural Network (RNN) for MNIST Classification

Found in the `jupyter-notebooks` folder. On the other hand, a separate folder for script implementations can be found in `scripts`.